<!---
APPLICATION FAMILY: CFBIBLE
APPLICATION SUB-FAMILY: PAGE
AUTHOR(S): SID WING
COPYRIGHT: SIDNEY WING - 2010
INCEPTION DATE: 14 AUGUST 2010
DATE LAST MODIFIED: 19 NOVEMBER 2010
LAST MODIFIED BY: SID WING
-----------------------------------
COMPONENT: RESULT2.CFM
PURPOSE: Display Chapter Texts as
result of Chapter being chosen
from the chapter list
-----------------------------------
--->


<!--- If page is being accessed form the Chapters Form --->
<cfif isDefined("FORM.fldBook")>


	<!--- Get All verses for Book/Chapter --->
    <cfset thisChapter = application.cfbSearch.getChapterText(FORM.fldBook, FORM.theChapter) />
    <!--- Get Short Name for the Book --->
    <cfset theBookName = application.cfbSearch.getBookByNumber(FORM.fldBook) />
    <!--- Get a list of all chapters in the book --->
    <cfset theChapters = application.cfbSearch.getBookChapters(FORM.fldBook) />
    <!--- Call layout and display results --->
    <cfset currentChapter = FORM.theChapter />
    <!--- If the currentChapter is not the LAST chapter --->
    <cfif currentChapter is not theChapters.recordcount>
    	<cfset nextChapter = FORM.theChapter + 1>
    </cfif>
    <!--- If the currentChapter is not the FIRST Chapter --->
    <cfif currentChapter is not 1>
    	<cfset prevChapter = FORM.theChapter - 1>
    </cfif>
    <!--- Display the Page --->
    <cfmodule template="modules/layout.cfm" title="CFBible">
    <h5>CFBIBLE - <cfoutput>#Ucase(theBookName.fldBookName)# - Chapter #FORM.theChapter#</cfoutput> - <cfif isDefined("prevChapter")><cfoutput><a href="result2.cfm?book=#form.fldbook#&chap=#prevChapter#" style="color:red;"><--- #prevChapter#</a></cfoutput>&nbsp;&nbsp;</cfif><cfif isDefined("nextChapter")><cfoutput><a href="result2.cfm?book=#form.fldbook#&chap=#nextChapter#" style="color:red;">#nextChapter# ---></a></cfoutput></cfif>  
    </h5>
    <span style="color:black;">
    <table border="1" cellpadding="10" cellspacing="0" bordercolor="black" bgcolor="white">
    <cfoutput query="thisChapter">
    <tr valign="top"><td>#FORM.theChapter#:#fldVerse#</td><td>#fldText#</td></tr>
    </cfoutput>
    </table>
    </span>
    </cfmodule>

<!--- Else if the page is being passed URL variables instead of FORM variables --->
<cfelse>

	<!--- Get All verses for Book/Chapter --->
    <cfset thisChapter = application.cfbSearch.getChapterText(URL.book, URL.chap) />
    <!--- Get Short Name for the Book --->
    <cfset theBookName = application.cfbSearch.getBookByNumber(URL.book) />
    <!--- Get a list of all chapters in the book --->
    <cfset theChapters = application.cfbSearch.getBookChapters(URL.book) />
    <!--- Call layout and display results --->
    <cfset currentChapter = URL.Chap />
    <!--- If the currentChapter is not the LAST chapter --->
    <cfif currentChapter is not theChapters.recordcount>
    	<cfset nextChapter = URL.Chap + 1>
    </cfif>
    <!--- If the currentChapter is not the FIRST Chapter --->
    <cfif currentChapter is not 1>
    	<cfset prevChapter = URL.Chap - 1>
    </cfif>
    <!--- Display the Page --->
    <cfmodule template="modules/layout.cfm" title="CFBible">
    <h5>CFBIBLE - <cfoutput>#Ucase(theBookName.fldBookName)# - Chapter #URL.chap#</cfoutput> - <cfif isDefined("prevChapter")><cfoutput><a href="result2.cfm?book=#URL.book#&chap=#prevChapter#" style="color:red;"><--- #prevChapter#</a></cfoutput>&nbsp;&nbsp;</cfif><cfif isDefined("nextChapter")><cfoutput><a href="result2.cfm?book=#URL.book#&chap=#nextChapter#" style="color:red; ">#nextChapter# ---></a></cfoutput></cfif>  
    </h5>
    <span style="color:black;">
    <table border="1" cellpadding="10" cellspacing="0" bordercolor="black" bgcolor="white"
    <cfoutput query="thisChapter">
    <tr valign="top"><td>#URL.chap#:#fldVerse#</td><td>#fldText#</td></tr>
    </cfoutput>
    </table>
    </span>
    </cfmodule>
</cfif>