<!---
APPLICATION FAMILY: CFBible
APPLICATION SUB-FAMILY: Application
AUTHOR(S): Sid Wing
COPYRIGHT: 2011, VWI
ORIGIN DATE: 20Apr2011
DATE LAST MODIFIED: 20Apr2011
LAST MODIFIED BY: Sid Wing
-----------------------------------
COMPONENT: Application.cfc
PURPOSE: Application Framework
-----------------------------------
FUNCTIONS:
-----------------------------------
onApplicationStart() - Runs on first
start of the application and sets all
application scoped functions, components
and variables.
onApplication() - Runs on application timeout 
and sets clears application scoped functions, 
components and variables from server memory
onSessionStart() - Runs on the start of any new 
session and sets session scoped variables and
functions.
onSessionEnd() - Runs on session timeout or end
and clears all session scoped variables/functions
onRequestStart() - Runs at the start of every
page request - and checks/sets request specific
variables.
--->



<cfcomponent>
	<!--- Set ApplicationCFC Variables --->	
	<cfset This.name = "CFBible" />
    <!--- Application Timeout: Default 2 days --->
	<cfset This.applicationTimeout = createTimeSpan(2,0,0,0) />
    <!--- Login Storage Mechanism: Default Session Storage --->
	<cfset This.loginStorage = "session" />
    <!--- Session Timeout: Default 20 min of inactivity --->
	<cfset This.Sessionmanagement=true />
	<cfset This.Sessiontimeout="#createtimespan(0,0,20,0)#" />
    <!--- Client Management --->
    <cfset This.clientmanagement="yes" />
    <cfset This.setclientcookies="yes" />
    <cfset This.clientstorage="cookie" />

    
    
	<cffunction name="onApplicationStart" access="public">
	<!--- Begin Reader section for Defaults.cfm --->
			<!--- Variable to hold the Default COnfig File Content --->
            <cfset var xmlFile = "">
            <!--- Variable to hold the XMLParsed Default Config File Content --->
            <cfset var xmlData = "">
            <!--- Variable to Fetch the Current Directory Path From Current Path --->
            <cfset var rootDir = getDirectoryFromPath(getCurrentTemplatePath())>
            <!--- Variable to hold Key index --->
            <cfset var key = "">
            <!--- Variable to hold the Default COnfig File Content --->
            <cfset var xmlSites = "">
            <!--- Variable to hold the XMLParsed Default Config File Content --->
            <cfset var xmlSitesData = "">
            <!--- Variable to Fetch the Current Directory Path From Current Path --->
            <cfset var sitesKey = "">
            <!--- load settings from Commented XML file --->
            <cffile action="read" file="#rootdir#/config/config.cfm" variable="xmlFile">
            <!--- Remove comments that are used to hide XML data from prying eyes--->
            <cfset xmlFile = replace(xmlFile, "<!---","")>
            <cfset xmlFile = trim(replace(xmlFile, "--->",""))>
            <!--- Parse the XML Data from rhe file --->
            <cfset xmlData = xmlParse(xmlFile)>
            <cfloop item="key" collection="#xmlData.initvals.defaults#">
                <cfset application[key] = xmlData.initvals.defaults[key].xmlText>
            </cfloop>
        <!--- End Reader section for Defaults.cfm --->
        
		<!--- Create Application COM Objects --->        
        <!---
		<cfset application.ddbAuth = createObject("component", "comps.auth").init(application.appdsn)>
        <cfset application.ddbLog = createObject("component", "comps.logger").init(application.appdsn)>--->
        <cfset application.cfbSearch = createObject("component", "comps.cfbSearch").init(application.appdsn)>
		
        <!--- End of COM Objects --->
        <!--- Log the Application start --->
        <!--- <cfset tmp = application.ddbLog.LogIT("1",cgi.REMOTE_ADDR,"Guest","APPLICATION START:",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) /> --->
	</cffunction>
    
    <cffunction name="OnApplicationEnd" access="public" returntype="void">
		<cfargument name="myArgument" type="string" required="yes">
		
	</cffunction>
 
    <!--- Create Base Session Variables and Structures --->
    <cffunction name="OnSessionStart" access="public">
    	<!--- Session start timestamp --->
		<cfset Session.started = DateFormat(now(),'mm-dd-yyyy') & " - " & TimeFormat(now(),'hh:mm:ss') />
        <!--- Session Login Object --->
        <cfset Session.login = StructNew() />
        <cfset Session.login.isLoggedIn = 0 />
        <cfset Session.login.loginCount = 0 />
        <!--- Create the default Session user object in the session scope --->
		<cfset Session.user = StructNew() />
        <cfset Session.user.username = "Guest" />
    	<cfset Session.user.password = "" />
        <!--- End User object creation --->
        <!--- Setup Button Counter and Struct --->
        <cfset Session.buttonStruct = StructNew() />
        <!--- Log the session start --->
        <!--- <cfset tmp = application.ddbLog.LogIT(session.sessionID,cgi.REMOTE_ADDR,"Guest","SESSION START: #session.started#",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) /> --->
        
	</cffunction>
    
    
    
    <!--- Clear Session Cookies and Variables ONSESSIONEND --->
    <cffunction name="OnSessionEnd" access="public" returntype="void">
    <cfargument name="sessionScope" type="struct" required="true">
	<cfargument name="appScope" type="struct" required="false">
    <!--- Log the Session End --->
    <!--- <cfset tmp = application.ddbLog.LogIT(session.sessionID, cgi.REMOTE_ADDR, session.user.username, 'SESSION END:', DateFormat(Now(),'mm-dd-yyyy'), TimeFormat(Now(),'HH:mm') ) /> --->
	    <cfcookie name="cfid" expires="now">
		<cfcookie name="cftoken" expires="now">
        <cfset clear = structDelete(session, started) />
        <cfset clear = structDelete(session.user, username) />
        <cfset clear = structDelete(session.user, password) />
	</cffunction>
    
   
	<cffunction name="onRequestStart" output="false">	
    	<!--- Check URL for the RE-INIT Command --->
    	<cfif isDefined("url.reinit")>
        	<!--- check to insure reinit KEY is correct --->
        	<cfif url.reinit is application.reinitkey>
            <!--- Log the Application reinit --->
	        <!--- <cfset tmp = application.ddbLog.LogIT(session.sessionID,cgi.REMOTE_ADDR,"Guest","APPLICATION REINIT:",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) /> --->
			<cfset onApplicationStart()>
            </cfif>
		</cfif>
        <!--- Check the form and CGI scope to see if request is a login attempt --->
		<cfif isDefined("FORM.bloo.x") and isDefined("FORM.bloo.y") and CGI.HTTP_REFERER contains "login.cfm">
        	<!--- Set Session variables for the lprocessor --->
        	<cfset session.LOGIN.unameATTEMPT = FORM.boo />
            <cfset session.LOGIN.pwordATTEMPT = FORM.hoo />
            <!--- Log the Login Attempt --->
	        <!--- <cfset tmp = application.ddbLog.LogIT(session.sessionID,cgi.REMOTE_ADDR,session.LOGIN.unameATTEMPT,"LOGIN ATTEMPT:",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) /> --->
        	<!--- Pass user to login processor --->
            <cflocation addtoken="no" url="lprocessor.cfm" />
        </cfif>
        <cfif isDefined("FORM.oolb")>
        	<!--- TODO: Add other logout Code here --->
            <!--- Log the Logout  --->
            <!--- <cfset tmp = application.ddbLog.LogIT(session.sessionID,cgi.REMOTE_ADDR,session.user.username,"LOGOUT:",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) /> --->
            <cfset tmp = application.ddbAuth.unBuildUser() />
            <cfset session.login.isLoggedIn = 0 />
        	<!--- Pass user to login page --->
            <cflocation addtoken="no" url="login.cfm" />
        </cfif>
        <cfif isDefined("URL.sidon1")>
			<!--- Session killer --->
            <cfif url.sidon1 is application.reinitkey>
	        <!--- <cfset tmp = application.ddbLog.LogIT(session.sessionID,cgi.REMOTE_ADDR,session.LOGIN.unameATTEMPT,"SESSION RESET:",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) />--->
            <cfset tmp = application.ddbAuth.unBuildUser() />
            <cfset session.login.isLoggedIn = 0 />
            <cfset session.login.loginCount = 0 />
        	<!--- Pass user to login page --->
            </cfif>
            <cflocation addtoken="no" url="login.cfm" />
        </cfif>
	</cffunction>
</cfcomponent>