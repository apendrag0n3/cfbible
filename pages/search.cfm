<!---
APPLICATION FAMILY: CFBIBLE
APPLICATION SUB-FAMILY: INCLUDE
AUTHOR(S): SID WING
COPYRIGHT: SIDNEY WING - 2010
INCEPTION DATE: 14 AUGUST 2010
DATE LAST MODIFIED: 19 NOVEMBER 2010
LAST MODIFIED BY: SID WING
-----------------------------------
COMPONENT: SEARCH.CFM
PURPOSE: Module to be included
in layout.cfm to add search 
capabilites.
-----------------------------------
--->
				<cfset theBooks = application.cfbSearch.getBooks()>
                <h4>Books</h4>
                <center>
                <form action="index.cfm" method="post">
				<select name="theBook">
                <cfif not isDefined("FORM.theBook")>
					<cfset huntBook = "gen">
				<cfelse>
					<cfset huntBook = FORM.theBook>
				</cfif>
					<cfoutput query="theBooks">
                    <option value="#theBooks.fldBookShort#"<cfif huntBook IS theBooks.fldBookShort>selected="yes"</cfif>>#theBooks.fldBookName#</option>
                    </cfoutput>
                    </select><br />
                    <input name="bookIT" value="Get Chapters" type="submit" />
                    </form>
                    </form>
                    </center>
                
                <cfif isDefined("FORM.bookIT")>
                <cfset theBookNumber = application.cfbSearch.getBookNumber(FORM.theBook) />
                <cfset theChapters = application.cfbSearch.getBookChapters(theBookNumber.fldRecID) />
                <h4>
                Chapters
                </h4>
                <center>
					<cfif isDefined("FORM.bookit")>
                        <span style="color:black;">
                        <form name="getChap" method="post" action="result2.cfm">
                        <select name="theChapter">
                        <cfoutput query="theChapters">
                        <option value="#fldChap#">Chapter #fldChap#</option>
                        </cfoutput>
                        </select><br />
                        <cfoutput>
                        <input type="hidden" name="fldBook" value="#theBookNumber.fldRecID#" />
                        </cfoutput>
                        <input type="submit" value="Show Chapter" name="pokeIT" />
                        </form>
                        </span>
                    </cfif>
                </center>
                </cfif>
				
