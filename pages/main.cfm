<h5>What Is CFBIBLE?</h5>
<p>A web-based application to provide the ability to search/read the text of the KJV of The Bible. 
Other versions/translations of The Bible will be made available as soon as I can insure there 
will be no issues with copyright violations.</p>
<p>We will, also, be adding in the ability to take notes, study guides, add bookmarks and more.</p>
<h5>NOTE TO FELLOW PROGRAMMERS:</h5>
<p>This is a ColdFusion-driven search engine with a basic database for easy installation.</p>
<p>The CFC that will drive this application will eventually allow for both local and remote access, API style versions of the CFC (coming soon).</p>