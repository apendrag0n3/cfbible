<!---
APPLICATION FAMILY: CFBIBLE
APPLICATION SUB-FAMILY: COMPONENT
AUTHOR(S): SID WING
COPYRIGHT: 2011, VWI
ORIGIN DATE: 20Apr2011
DATE LAST MODIFIED: 20Apr2011
LAST MODIFIED BY: Sid Wing
-----------------------------------
COMPONENT: cfbSearch.CFC
PURPOSE: Search utility component
-----------------------------------
FUNCTIONS:
INIT - Inits the CFC into Application memory space
GETTEXT - Return all applicable text after searching all verses
GETBOOKS - Return all Books (From 1-66)
GETBOOKNUMBER - Get Book Number based on short name of book
GETBOOKBYNUMBER - Get Book Short Name based on number of book
GETBOOKCHAPTERS - Return all chapters numbers for a given book
GETCHAPTERTEXT - Return all verses for a given chapter in a given book
--->
<cfcomponent displayname="cfbSearch" hint="I manage data interactions with the database. I can be used to handle db search functions.">
	<cfset variables.dsn = "cfbible" />
    
	<!--- Init Function --->
    <cffunction name="init" access="public" returntype="cfbSearch" output="no" hint="I instantiate and return this object.">
    	<cfargument name="theDSN" type="string" required="true" />
        <cfset variables.dsn = arguments.theDSN />
        <cfreturn this>
    </cffunction>	
    
    <cffunction name="getText" access="public" returntype="query" hint="Return all applicable text after searching all verses">
		<cfargument name="srchText" type="string" required="yes" hint="Text to search for"> 
		<cfquery datasource="#variables.dsn#" name="myResult">
        SELECT *
        FROM tblKJV
        WHERE fldText LIKE '%#arguments.srchText#%'
        ORDER BY fldBook, fldChap, fldVerse
		</cfquery>
		<cfreturn myResult />
	</cffunction>

	<cffunction name="getBooks" access="public" returntype="query" hint="Return all Books (From 1-66)">
		<cfquery datasource="#variables.dsn#" name="myResult">
        SELECT fldRecID, fldBookName, fldBookShort
        FROM tbKJVBooks
        WHERE fldRecId <= 66
		</cfquery>
		<cfreturn myResult />
	</cffunction>
    
    <cffunction name="getBookByNumber" access="public" returntype="query"  hint="Get Book Number based on BookID">
    	<cfargument name="theBookID" type="numeric" required="yes" hint="Short Name of the book" />
		<cfquery datasource="#variables.dsn#" name="myResult">
        SELECT fldBookShort, fldBookName
        FROM tbKJVBooks
        WHERE fldRecID = #arguments.theBookID#
		</cfquery>
		<cfreturn myResult />
	</cffunction>
    
    <cffunction name="getBookNumber" access="public" returntype="query"  hint="Get Book Number based on short name of book">
    	<cfargument name="theBookID" type="string" required="yes" hint="Short Name of the book" />
		<cfquery datasource="#variables.dsn#" name="myResult">
        SELECT fldRecID
        FROM tbKJVBooks
        WHERE fldBookShort = '#arguments.theBookID#' 
		</cfquery>
		<cfreturn myResult />
	</cffunction>
    
    <cffunction name="getBookChapters" access="public" returntype="query" hint="Return all chapters numbers for a given book">
		<cfargument name="theBookID" type="numeric" required="yes" hint="The Record ID of the Book">
        <cfquery datasource="#variables.dsn#" name="myResult">
        SELECT DISTINCT fldChap
        FROM tblKJV
        WHERE fldBook = #arguments.theBookID#
        ORDER BY fldChap
        </cfquery>
        <cfreturn myResult />
	</cffunction>
    
    <cffunction name="getChapterText" access="public" returntype="query" hint="Return all verses for a given chapter in a given book">
    	<cfargument name="theBookID" type="numeric" required="yes" hint="The Record ID of the Book">
        <cfargument name="theChapID" type="numeric" required="yes" hint="The Number of the Chapter in the Book">
        <cfquery datasource="#variables.dsn#" name="myResult">
        SELECT fldRecID, fldBook, fldChap, fldVerse, fldText
        FROM tblKJV
        WHERE fldBook = #arguments.theBookID# AND fldChap = #arguments.theChapID#
        ORDER BY fldVerse
        </cfquery>
    	<cfreturn myResult />
    </cffunction>
</cfcomponent>