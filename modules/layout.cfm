<!---
APPLICATION FAMILY: CFBIBLE
APPLICATION SUB-FAMILY: TAG
AUTHOR(S): SID WING
COPYRIGHT: 2011, VWI
ORIGIN DATE: 20Apr2011
DATE LAST MODIFIED: 20Apr2011
LAST MODIFIED BY: Sid Wing
-----------------------------------
COMPONENT: LAYOUT.CFM
PURPOSE: Page Layout and construction
-----------------------------------
--->
<cfif thisTag.executionMode is "start">
    <cfsetting showdebugoutput="no" />
    <cfparam name="attributes.title" default="CFBIBLE">
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><cfoutput>#attributes.title#</cfoutput></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <link href="styles/styles.css" rel="stylesheet" type="text/css" media="screen" />
    </head>
    <body>
        <div id="content">
            <!-- header begins -->
            <div id="headertop">
                <div id="menu">
                    <ul>
                        <li id="button1"><a href="index.cfm" title="">Home</a></li>
                        <li id="button2"><a href="index2.cfm" title="">Guides</a></li>
                        <li id="button3"><a href="index2.cfm" title="">Notes</a></li>
                        <li id="button4"><a href="index2.cfm" title="">About</a></li>
                        <li id="button5"><a href="index2.cfm" title="">Contact</a></li>
                    </ul>
                </div>
                <div id="logo">
                    <h1><span style="color:white;">CFBible</span></h1>
                    <h2><span style="color:white;">Online Bible Reference</span></h2>
                </div>

            </div><div style="clear:both;"></div>
            <div id="header">
                <div id="logform">
                	<!---
                    <div id="log_top"></div>
                    <div id="log">
                        <h3>User Login</h3>
                        <form id="form1" method="post" action="#">
                            <fieldset>
                                <label for="text1">Username</label><br />
                                <input id="text1" type="text" name="text1" value="" /><br />
                                <label for="text2">Password</label><br />
                                <input id="text2" type="password" name="text2" value="" /><br />
                                <input type="submit" id="login-submit" value="" />
                            </fieldset>
                        </form>
                        <img src="imgs/log_ls.png" title="" alt="" style="padding-right: 5px; padding-bottom: 2px;"/><a href="#">Create new account</a><br />
                        <img src="imgs/log_ls.png" title="" alt="" style="padding-right: 5px; padding-bottom: 2px;"/><a href="#">Request new password</a>
                    </div>
                    <div id="log_bot"></div> --->
					<div id="search">
                        <form action="result1.cfm" method="post">
                            <fieldset>
                            	<input value="Search text" size="15" name="mySearch" type="text" maxlength="255" id="search-text" />
                                <!---<input type="text" name="s" id="search-text" size="15" />--->
                                <input type="hidden" name="findIT" value="FIND" />
                                <input type="submit" id="search-submit" value="" />
                            </fieldset>
                        </form>
                    </div></div>
                <!-- end #search -->

            </div>
            <!-- header ends -->
            <!-- content begins -->
            <div id="main">
                <div id="leftcon">
<cfelse>
                </div>
                
                <div id="rightcon">
					<cfinclude template="../pages/search.cfm" />
                </div>
                <div style="clear:both;"></div>
                <!--content ends -->
                <!--footer begins -->
            </div>
            <div id="con_bot"></div>
        <div id="footer">
            <p>Copyright  2011. <!---<a href="#">Privacy Policy</a> | ---><a href="#">Terms of Use</a> | <a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional"><abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a> | <a href="http://jigsaw.w3.org/css-validator/check/referer" title="This page validates as CSS"><abbr title="Cascading Style Sheets">CSS</abbr></a></p>
            <!--- <p>Design by <a href="http://www.flashtemplatesdesign.com/" title="Free Flash Templates">Free Flash Templates</a></p> --->
        </div>
   </div>
        <!-- footer ends-->
    </body>
</html>
</cfif>
