<!---
APPLICATION FAMILY: CFBIBLE
APPLICATION SUB-FAMILY: PAGE
AUTHOR(S): SID WING
COPYRIGHT: 2011, VWI
ORIGIN DATE: 20Apr2011
DATE LAST MODIFIED: 20Apr2011
LAST MODIFIED BY: Sid Wing
-----------------------------------
COMPONENT: INDEX.CFM
PURPOSE: START PAGE
-----------------------------------
--->
<cfmodule template="modules/layout.cfm" title="CFBible">
	<cfdiv id="theBox" bind="url:pages/main.cfm">
    
    </cfdiv>
</cfmodule>