<!---
APPLICATION FAMILY: DDB1
APPLICATION SUB-FAMILY: PAGE
AUTHOR(S): Sid Wing
COPYRIGHT: 2011, YMS
ORIGIN DATE: 1Mar2011
DATE LAST MODIFIED: 12Apr2011
LAST MODIFIED BY: Sid Wing
-----------------------------------
COMPONENT: Login.cfm
PURPOSE: Login Page
-----------------------------------
--->
<cfmodule template="modules/layout.cfm" title="DDB Login">
  <cfif session.login.loginCount LTE 3>
  <cfform name="DaBoo" method="post" action="login.cfm">
  <table align="center" border="0" cellpadding="0" cellspacing="0" width="450" style="color:white;">
  <!--- Get Branding and display appropriate logo --->
  <cfswitch expression="#session.brand#">
  		<cfcase value="RDS">
			<tr><th align="center" colspan="2" ><img src="imgs/rdslogo.png" align="middle" style="padding-top:55px; padding-bottom:5px;" /></th></tr>
        </cfcase>
        <cfcase value="YMS">
			<tr><th align="center" colspan="2" ><img src="imgs/ymslogo.png" align="middle" style="padding-top:55px; padding-bottom:5px;" /></th></tr>
        </cfcase>
        <cfcase value="YAI">
			<tr><th align="center" colspan="2" ><img src="imgs/yailogo.png" align="middle" style="padding-top:55px; padding-bottom:5px;" /></th></tr>
        </cfcase>
        <cfcase value="JVYS">
			<tr><th align="center" colspan="2" ><img src="imgs/jvyslogo.png" align="middle" style="padding-top:55px; padding-bottom:5px;" /></th></tr>
        </cfcase>
        <cfdefaultcase>
        	<tr><th align="center" colspan="2" ><img src="imgs/logo1.jpg" align="middle" style="padding-top:55px; padding-bottom:5px;" width="100" /></th></tr>
        </cfdefaultcase>
  </cfswitch>
  <!--- End Branding --->
  <tr><th align="center" colspan="2" ><img src="imgs/logo.gif" align="middle" style="padding-bottom:15px;" /></th></tr>
  <cfif isDefined("URL.xd")>
  <tr><td align="right">LOGIN ID</td><td align="left"><cfinput type="text" name="boo" value="" size="25" maxlength="55" required="yes" message="LOGIN ID Required" /></td></tr>
  <cfelse>
  <tr><td align="right">LOGIN ID</td><td align="left"><cfinput type="text" name="boo" value="" size="25" maxlength="55" required="yes" message="LOGIN ID Required" /></td></tr>
  </cfif>
  <tr><td align="right">PASSWORD</td><td align="left"><cfinput type="password" name="hoo" value="" size="27" maxlength="55" required="yes" message="PASSWORD Required" /></td></tr>
  <tr><th colspan="2"><cfinput border="0" type="image" src="imgs/login1.png" name="bloo" value="submit" onMouseOver="this.src='imgs/login2.png';" onMouseOut="this.src='imgs/login1.png';" /></th></tr>
  <cfif isDefined("URL.xd")>
  <tr><th colspan="2"><span style="text-align:center; font-size:14px; color:red;">LOGIN FAILED!</span></th></tr>
  </cfif>
  <tr><th colspan="2" style="background-color:white; border-style:inset"><span style="font-size:9px; color:red;">WARNING: This computer system is "FOR OFFICIAL USE ONLY" and may only be used to store/process information that is UNCLASSIFIED. 
  This system is subject to monitoring by Security Personnel. Therefore, no expectation of privacy is to be assumed. Individuals found performing unauthorized activities 
  are subject to disciplinary action including criminal prosecution.</span></th></tr>
  </table>
  </cfform>
  <cfelse>
  <table align="center" border="0" cellpadding="0" cellspacing="0" width="450" style="color:white;">
  <cfif session.brand contains "RDS">
	  <tr><th align="center" colspan="2" ><img src="imgs/rdslogo.png" align="middle" style="padding-top:55px; padding-bottom:5px;" /></th></tr>
  <cfelse>
  	  <tr><th align="center" colspan="2" ><img src="imgs/logo1.jpg" align="middle" style="padding-top:55px; padding-bottom:5px;" width="100" /></th></tr>
  </cfif>
  <tr><th colspan="2"><span style="text-align:center; font-size:14px; color:red;">Your Login attempts have failed.  This system has now locked out this account.</span></th></tr>
  <cfif isDefined("URL.xd")>
  <tr><th colspan="2"><span style="text-align:center; font-size:14px; color:red;">LOGIN FAILED!</span></th></tr>
  </cfif>
  <tr><th colspan="2" style="background-color:white; border-style:inset"><span style="font-size:9px; color:red;">WARNING: This computer system is "FOR OFFICIAL USE ONLY" and may only be used to store/process information that is UNCLASSIFIED. 
  This system is subject to monitoring by Security Personnel. Therefore, no expectation of privacy is to be assumed. Individuals found performing unauthorized activities 
  are subject to disciplinary action including criminal prosecution.</span></th></tr>
  </table>
  </cfif>
</cfmodule>