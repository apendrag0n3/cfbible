<!---
APPLICATION FAMILY: DDB1
APPLICATION SUB-FAMILY: PAGE
AUTHOR(S): Sid Wing
COPYRIGHT: 2011, YMS
ORIGIN DATE: 1Mar2011
DATE LAST MODIFIED: 4APR2011
LAST MODIFIED BY: Sid Wing
-----------------------------------
COMPONENT: index.cfm
PURPOSE: Main Home Page
-----------------------------------
--->
<cfmodule template="modules/layout.cfm" title="DDB Main">
<cfdiv align="center" id="boxContent" bind="url:pages/pmTools.cfm">

</cfdiv>
<!--- Diagnostic stuff --->
<!---
<cfdump var="#session.buttonStruct#" />
<cfdump var="#session.user#" />
<cfdump var="#cgi#" />
  <cfdump var="#session#" />
--->

</cfmodule>