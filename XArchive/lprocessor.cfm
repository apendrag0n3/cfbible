<!---
APPLICATION FAMILY: DDB1
APPLICATION SUB-FAMILY: PAGE
AUTHOR(S): Sid Wing
COPYRIGHT: 2011, YMS
ORIGIN DATE: 8Mar2011
DATE LAST MODIFIED: 4Apr2011
LAST MODIFIED BY: Sid Wing
-----------------------------------
COMPONENT: lprocessor.cfm
PURPOSE: Login Processor
-----------------------------------
--->
<!--- Make sure nothing gets output to the browser by this page --->
<cfsetting enablecfoutputonly="yes" />
<!--- Function to convert binary arrays to strings --->
<cfscript>
function guidToString(guidByteArray) {
   var hexString='';
   
   if (IsArray(guidByteArray) AND ArrayLen(guidByteArray) GTE 16) {
     hexString=hexString & guidByteToHex(guidByteArray[4]);
     hexString=hexString & guidByteToHex(guidByteArray[3]);
     hexString=hexString & guidByteToHex(guidByteArray[2]);
     hexString=hexString & guidByteToHex(guidByteArray[1]);
     hexString=hexString & "-";
     hexString=hexString & guidByteToHex(guidByteArray[6]);
     hexString=hexString & guidByteToHex(guidByteArray[5]);
     hexString=hexString & "-";
     hexString=hexString & guidByteToHex(guidByteArray[8]);
     hexString=hexString & guidByteToHex(guidByteArray[7]);
     hexString=hexString & "-";
     hexString=hexString & guidByteToHex(guidByteArray[9]);
     hexString=hexString & guidByteToHex(guidByteArray[10]);
     hexString=hexString & "-";
     hexString=hexString & guidByteToHex(guidByteArray[11]);
     hexString=hexString & guidByteToHex(guidByteArray[12]);
     hexString=hexString & guidByteToHex(guidByteArray[13]);
     hexString=hexString & guidByteToHex(guidByteArray[14]);
     hexString=hexString & guidByteToHex(guidByteArray[15]);
     hexString=hexString & guidByteToHex(guidByteArray[16]);
   }
   
   return hexString;
}

function guidByteToHex(guidByte) {
   var hexByte=Ucase(Right(FormatBaseN(guidByte, 16),2));

   if (Len(hexByte) IS 0) {
      hexByte='00';
   } else if (Len(hexByte) IS 1) {
      hexByte='0' & hexByte;
   }
   
   return hexByte;
}
</cfscript>
<!--- End Conversion --->


<!--- If being called from the login page --->
<cfif cgi.HTTP_REFERER contains "login.cfm" and isDefined("session.login.unameATTEMPT")>

	
	<!--- Update the login attempt counter --->
    <cfset session.login.loginCount = session.login.loginCount + 1>
    <!--- Does user exist in YMS Domain --->
    <cfset ymsUserCheck = application.ddbAuth.ymsSAMLookup(session.login.unameATTEMPT) />
	<!--- Does user exist in GCS Domain --->
    <cfset gcsUserCheck = application.ddbAuth.gcsSAMLookup(session.login.unameATTEMPT) />
	
	
	<!--- YMS AUTH ROUTINES --->
    <!--- If they exist in YMS - check username and password combo --->
    <cfif ymsUserCheck.recordcount is 1>
	    <cfset ymsUser = application.ddbAuth.ymsTest(session.login.unameATTEMPT, session.login.pwordATTEMPT) />
            <!--- if they pass the user/pass check - then build their user object and mark them as logged in --->
			<cfif ymsUser.recordcount is 1>
            	<cfset tmp = application.ddbAuth.BuildUser(ymsUser,"YMS") />
                <!--- convert binaries to strings --->
                <cfset session.user.userGUID = guidToString(session.user.userGUID) />
                <cfset session.user.userSID = guidToString(session.user.userSID) />
                		<!--- Is user in the Database --->
                        <cfset tmp2 = application.ddbAuth.getUser(session.user.userGUID) />
                        <!--- If not, add them to the Database --->
						<cfif tmp2.recordcount is 0>
                            <cfset tmp3 = application.ddbAuth.addUser(session.user) />
                        </cfif>
                <cfset session.login.loginCount = 0 />
                <cfset session.login.isLoggedIn = 1 />
                <cfset tmp = application.ddbLog.LogIT("1",cgi.REMOTE_ADDR,session.user.username,"LOGIN: #session.user.username#",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) />
                <cflocation addtoken="no" url="index.cfm">
            </cfif>
    <!--- GCS AUTH ROUTINES --->
    <!--- If they exist in YMS - check username and password combo --->
    <cfelseif gcsUserCheck.recordcount is 1>
	    <cfset gcsUser = application.ddbAuth.gcsTest(session.login.unameATTEMPT, session.login.pwordATTEMPT) />
            <!--- if they pass the user/pass check - then build their user object and mark them as logged in --->
			<cfif gcsUser.recordcount is 1>
            	<cfset tmp = application.ddbAuth.BuildUser(gcsUser,"GCS") />
                <!--- convert binaries to strings --->
                <cfset session.user.userGUID = guidToString(session.user.userGUID) />
                <cfset session.user.userSID = guidToString(session.user.userSID) />
                		<!--- Is user in the Database --->
                        <cfset tmp2 = application.ddbAuth.getUser(session.user.userGUID) />
                        <!--- If not, add them to the Database --->
						<cfif tmp2.recordcount is 0>
                            <cfset tmp3 = application.ddbAuth.addUser(session.user) />
                        </cfif>
                <cfset session.login.loginCount = 0 />
                <cfset session.login.isLoggedIn = 1 />
                <cfset tmp = application.ddbLog.LogIT("1",cgi.REMOTE_ADDR,session.user.username,"LOGIN: #session.user.username#",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) />
                <cflocation addtoken="no" url="index.cfm">
            </cfif>
    <!--- If neither YMS or GCS user --->
    <cfelse>
    	<!--- Log the failed attempt --->
		<cfset tmp = application.ddbLog.LogIT("1",cgi.REMOTE_ADDR,session.login.unameATTEMPT,"LOGIN FAILED: #session.login.unameATTEMPT#",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) />
		<!--- Encrypt the username --->
		<cfset tehUser = Encrypt(session.login.unameATTEMPT,application.failkey,'CFMX_COMPAT','Hex')>
        <!--- Send back to the login page --->
        <cflocation url="login.cfm?xd=#tehUser#" addtoken="no">
    </cfif>
<!--- if the login page did not call this page --->
<cfelse>
	<!--- Log the failed attempt --->
	<cfset tmp = application.ddbLog.LogIT("1",cgi.REMOTE_ADDR,session.login.unameATTEMPT,"LOGIN FAILED: #session.login.unameATTEMPT#",DateFormat(Now(),'mm-dd-yyyy'),TimeFormat(Now(),'HH:mm')) />
    <cfset session.login.loginCount = session.login.loginCount + 1>
    <cflocation addtoken="no" url="login.cfm">


</cfif>