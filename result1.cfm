<!---
APPLICATION FAMILY: CFBIBLE
APPLICATION SUB-FAMILY: PAGE
AUTHOR(S): SID WING
COPYRIGHT: SIDNEY WING - 2010
INCEPTION DATE: 14 AUGUST 2010
DATE LAST MODIFIED: 18 NOVEMBER 2010
LAST MODIFIED BY: SID WING
-----------------------------------
COMPONENT: RESULT1.CFM
PURPOSE: DISPLAY RESULTS FROM 
FREE-FORM TEXT SEARCH FORM
-----------------------------------
--->

<!--- Search DB with text supplied from Text search form --->
<cfset TheSearch = application.cfbSearch.getText(FORM.mySearch) />

<!--- Call layout and display results --->
<cfmodule template="modules/layout.cfm" title="CFBible">
<h5>CFBIBLE - Search Results</h5>
<span style="color:black;">
<table border="1" cellpadding="0" cellspacing="0" bordercolor="black" bgcolor="white" width="100%">
<tr><th>Book</th><th>Chap</th><th>Verse</th><th>Text</th></tr>
<cfoutput query="TheSearch">
<cfset theBook = application.cfbSearch.getBookByNumber(fldBook)>
<tr valign="top"><td>#UCase(theBook.fldBookShort)#</td><td>#fldChap#</td><td>#fldVerse#</td><td>#fldText#</td></tr>
</cfoutput>
</table>
</span>
</cfmodule>